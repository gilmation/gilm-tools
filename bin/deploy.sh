#!/bin/bash
#
# The script that knows how to deploy a new version of the morphsuits website
#

# Check the args
ARGS_EXPECTED=1

if [[ $# -ne $ARGS_EXPECTED ]]; then
  echo "Usage: `basename $0` deployment_tag"
  exit 1
fi

#
# The setup of the deployment directories should be as follows
#
# Standard Variables
CURRENT=
GIT_REPO=git@github.com:morphsuits/magento_website.git

# Directory Variables
RELEASES=releases
SHARED=shared
MORPH_DEPLOY_HOME=/home/vhosts/morphsuits
SYMLINKS='media var'

# Rsyncing the contents of the current release folder to the new release folder
# Update the crontab entry

# Updating the contents of the new release folder via svn update / svn switch.
# git fetch + git checkout
cd $MORPH_DEPLOY_HOME/$RELEASES
git clone $GIT_REPO $CURRENT
cd $CURRENT
git reset --hard $1

# Permissions
sudo chgrp -R apache $MORPH_DEPLOY_HOME/$RELEASES/$CURRENT

# Symlink for shared directories
for link in $SYMLINKS
do
  ln -snf $MORPH_DEPLOY_HOME/$SHARED/$link $MORPH_DEPLOY_HOME/$RELEASES/$CURRENT/$link
done

# Modifying the symlink to point to the new release
# ln -snf $MORPH_DEPLOY_HOME/$RELEASES/$CURRENT $MORPH_DEPLOY_HOME/live
