#!/bin/bash
today=`date +%s`
for cert in `grep "SSLCertificate\(Chain\)*File" /etc/httpd/conf/httpd.conf | awk '{print $2}'`; do
  expDate=`openssl x509 -in $cert -noout -enddate | cut -d "=" -f 2`
  exp=`date -d "$expDate" +%s`; remaining=$[exp-today]
  # Alert when less or equal than 15 days
  if [ $remaining -lt 1296000 ]; then 
    if [ $remaining -lt 0 ]; then
      event="EXPIRED!!"
    else
      event="expires soon"
    fi
    echo "$cert $event ($expDate)" 
  fi
done
