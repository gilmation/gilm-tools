#!/bin/bash

server=`hostname | cut -d "." -f 1`
date=`date +'%F %H:%M'`

if [ $server == 'rx204205' ]; then
  node='morph-master'
  pos="5"
elif [ $server == 'rx186030' ]; then
  node='morph4'
  pos="1"
elif [ $server == 'rx187031' ]; then
  node='morph5'
  pos="2"
elif [ $server == 'rx187203' ]; then
  node='morph6'
  pos="3"
elif [ $server == 'rx202071' ]; then
  node='morph7'
  pos="4"
elif [ $server == 'rx197091' ]; then
  node='trade-sales'
  pos="6"
else
  node=$server
  pos="?"
fi
echo "Email sent from $node($server) at $date" | mail -s "Email active in $node - $pos/6 - ($date)" prod_email_test@morphsuits.co.uk
