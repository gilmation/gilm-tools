#!/bin/bash

# script_launcher accepts a configuration file as a parameter (as well as --dryrun optionally)
# and launches the commands specified in the configuration file properties:
#
# site: morphsuits/foulfashion/royalandawesome ... It tells the script where to find local.xml
# title: Email subject (if mail is set)
# server: Where to launch the shell script (it needs ssh public key trust configuration)
# script: Shell script
# query: SQL query (update / alter commands are not executed)
# mail: Email recipients of the query / script results
# output: File output (suffixed with the launch date)
# csv: yes/no. Attach result (only for queries) as a comma-separated CSV file

function readParam() {
  grep "^"$1"=" $2 | cut -d "=" -f 2-
}

function readConfigFile() {

  site=`readParam site $1`
  title=`readParam title $1`
  script=`readParam script $1`
  output=`readParam output $1`
  server=`readParam server $1`
  query=`readParam query $1`
  mail=`readParam mail $1`
  csv=`readParam csv $1`
  if [ "x"$csv == "xyes" ]; then
    csv=1
  else
    csv=0
  fi

  if [ "x"$site == "x" ]; then
    echo "ERROR: Unable to read script site"
    exit -1
  fi
}

function showConfig() {
  echo "site    ["$site"]"
  echo "script  ["$script"]"
  echo "output  ["$output"]"
  echo "servers ["${server[$*]}"]"
  echo "query   ["$query"]"
  echo "title   ["$title"]"
  echo "mail    ["$mail"]"
  echo "csv     ["$csv"]"
}

function checkQuery() {
  echo $query | tr "[:upper:]" "[:lower:]" | egrep -q "\<alter table\>
\<insert\>
\<update\>
\<delete\>
\<truncate\>
\<create\>
\<drop\>"

  if [ $? -eq 0 ]; then
    echo "ERROR: The query was considered harmful and was not executed"
    error=1
  fi
}

function readInstanceDbConnection() {
  error=0
  DB_HOST=`grep "<host>" $1  | cut -d "[" -f 3 | cut -d "]" -f 1`
  DB_NAME=`grep "<dbname>" $1  | cut -d "[" -f 3 | cut -d "]" -f 1`
  DB_USER=`grep "<username>" $1  | cut -d "[" -f 3 | cut -d "]" -f 1`
  DB_PASSWORD=`grep "<password>" $1  | cut -d "[" -f 3 | cut -d "]" -f 1`

  if [ "x"$DB_HOST == "x" ]; then
    echo "ERROR: Unable to read database host"
    error=1
  fi
  if [ "x"$DB_NAME == "x" ]; then
    echo "ERROR: Unable to read database name"
    error=1
  fi
  if [ "x"$DB_USER == "x" ]; then
    echo "ERROR: Unable to read database user"
    error=1
  fi
  if [ "x"$DB_PASSWORD == "x" ]; then
    echo "ERROR: Unable to read database password"
    error=1
  fi
}

function executeQuery() {
  readInstanceDbConnection /home/vhosts/$site/live/app/etc/local.xml
  checkQuery
  if [ $error -eq 1 ]; then
    return
  fi

  optFile=`mktemp`
  chmod 600 $optFile
  {
    echo "[client]"
    echo "password=$DB_PASSWORD"  
  } > $optFile
  echo $query | mysql --defaults-file=$optFile -A -h $DB_HOST -u $DB_USER $DB_NAME
  rm -f $optFile
}

function executeScript() {
local s

  IFS=" 	"
  for s in $server; do
    echo
    echo "Server $s"
    echo "===="
    if [ -a $script ]; then
      if [ $s == "localhost" ]; then
        cp $script /tmp
        scriptName=`basename $script`
        chmod +x /tmp/$scriptName; /tmp/$scriptName
        rm -f /tmp/$scriptName
      else
        scp -P 222 $script $s:/tmp
        scriptName=`basename $script`
        ssh -p 222 $s "chmod +x /tmp/$scriptName; /tmp/$scriptName"
        ssh -p 222 $s "rm -f /tmp/$scriptName"
      fi
    else
      echo "ERROR: Unable to find script " $script
    fi
  done
  IFS="
"
}

function syntaxError() {
  echo "ERROR: Syntax `basename $0` [--dryrun] script_config_file"
  exit -1
}

function parseParameters() {
  if [ $# -lt 1 ] || [ $# -gt 2 ]; then
    syntaxError
  fi

  DRYRUN=0
  if [ $1 == "--dryrun" ]; then
    DRYRUN=1
    shift
    if [ $# -eq 1 ]; then
      CFG=$1
    else
      syntaxError
    fi
  else
    CFG=$1
    shift
    if [ $# -eq 1 ]; then
      if [ $1 == "--dryrun" ]; then
        DRYRUN=1
      else
        syntaxError
      fi
    fi
  fi
}



parseParameters $*

originalIFS=$IFS
IFS="
"
declare -a server
readConfigFile $CFG
if [ $DRYRUN -eq 1 ]; then
  showConfig
  exit 0
fi

outFile=`mktemp`
if [ "x"$query != "x" ]; then
  executeQuery >> $outFile 2>&1
  size=`stat -c %s $outFile`
  empty=0
  if [ $csv -eq 1 ]; then
    if [ $size -gt $empty ]; then
      sed 's/\t/","/g' $outFile | sed 's/^/"/g' | sed 's/$/"/g' > /tmp/script_launcher_csv.tmp
      mv /tmp/script_launcher_csv.tmp $outFile
    fi
  fi
fi

if [ "x"$script != "x" ];then
  executeScript >>$outFile 2>&1
  size=`wc -l $outFile | cut -d " " -f 1`
  empty=`echo $server | wc -w`
  empty=$[3*empty]
fi

now=`date +%Y%m%d_%H%M%d%S`

if [ $size -gt $empty ]; then
  if [ "x"$mail != "x" ]; then
    IFS=$originalIFS
    if [ $csv == 1 ]; then
      mv $outFile /tmp/query-$now.csv
      echo "[$site] $title" | mutt -s "[$site] $title" -a /tmp/query-$now.csv $mail 
    else
      echo >> $outFile
      date >> $outFile
      cat $outFile | mutt -s "[$site] $title" $mail 
    fi
  fi
  if [ "x"$output != "x" ]; then
    cp $outFile $output.$now
  fi
fi

rm -f $outFile /tmp/query-$now.csv
