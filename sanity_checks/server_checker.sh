#!/bin/bash

# script_launcher accepts a configuration file as a parameter (as well as --dryrun optionally)
# and launches the commands specified in the configuration file properties:
#
# site: morphsuits/foulfashion/royalandawesome ... It tells the script where to find local.xml
# title: Email subject (if mail is set)
# server: Where to launch the shell script (it needs ssh public key trust configuration)
# script: Shell script
# query: SQL query (update / alter commands are not executed)
# mail: Email recipients of the query / script results
# output: File output (suffixed with the launch date)
# csv: yes/no. Attach result (only for queries) as a comma-separated CSV file


function executeScript() {
local s

  IFS=" 	"
  for s in $server; do
    echo
    echo "Server $s"
    echo "===="
    if [ -a $script ]; then
      if [ $s == "localhost" ]; then
        cp $script /tmp
        scriptName=`basename $script`
        chmod +x /tmp/$scriptName; /tmp/$scriptName
        rm -f /tmp/$scriptName
      else
        scp -P 222 $script $s:/tmp
        scriptName=`basename $script`
        ssh -p 222 $s "chmod +x /tmp/$scriptName; /tmp/$scriptName"
        ssh -p 222 $s "rm -f /tmp/$scriptName"
      fi
    else
      echo "ERROR: Unable to find script " $script
    fi
  done
  IFS="
"
}

function syntaxError() {
  echo "ERROR: Syntax `basename $0` checks_file compare_servers(0/1) servers_list"
  exit -1
}

function parseParameters() {
  if [ $# -lt 3 ]; then
    syntaxError
  fi

  DRYRUN=0
  if [ $1 == "--dryrun" ]; then
    DRYRUN=1
    shift
  fi

  if [ $# -lt 3 ]; then
    syntaxError
  fi

  checks=$1
  shift
  compare=$1
  shift
  servers=$*
}


declare -a servers

parseParameters $*

rm -f /tmp/the_servers
for server in $servers; do
  mkdir -p ~/server_checks/$server/{new,latest}
  echo $server >> /tmp/the_servers
done

IFS="
"
for server in `cat /tmp/the_servers`; do
echo $server
  i=0
  for line in `cat $checks | grep -v "^#"`; do
echo ::$line
    i=$[i+1]
    if [ "x"$line == "x" ]; then
      continue
    fi
    # Execute
    if [ $server == "localhost" ]; then
      eval $line 
    else
      ssh -p 222 $server "$line" 
    fi > ~/server_checks/$server/new/$i

    # Compare and show differences (if any)
    if [ -a ~/server_checks/$server/latest/$i ]; then
      diff ~/server_checks/$server/new/$i ~/server_checks/$server/latest/$i > /tmp/difference
      dif=`wc -l /tmp/difference | awk '{print $1}'`
      if [ $dif -ne 0 ]; then
        echo $server:$line
        cat /tmp/difference
      fi
      rm -f /tmp/difference 
    fi
  done
done

if [ $compare -eq 1 ]; then
  i=0  
  for comps in ~/server_checks/$server/new/*; do
    if [ $comps -eq 0 ]; then
      base=$comps
    else
      diff $base $comps
    fi
    i=$[i+1]
  done
fi

for server in `cat /tmp/the_servers`; do
  rm -rf ~/server_checks/$server/latest
  mv ~/server_checks/$server/new ~/server_checks/$server/latest
done
rm -f /tmp/the_servers
