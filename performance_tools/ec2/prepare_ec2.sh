#!/bin/bash
#
# In order to reproduce production in Amazon EC2 we need:
#
# - A server for Varnish / Pound (i-d0d38399).
#   Pound backend is i-d0d38399 internal IP (Varnish)
#   Varnish backends are i-cc1f81ba and i-9c93c7ea (Apache)
# - 2 Apache servers
#   Their backend is db: i-7ae4b933 internal IP (MySQL)
# - A MySQL server 
#   With morphsuits@node1 and morphsuits@node2 users (node1 and node2 are internal server names)   


function ensure_cached() {
  if [ ! -f /tmp/$1 ]; then
    ec2-describe-instances $1 > /tmp/$1
  fi
}

function internal_ip() {
  ensure_cached $1
  grep INSTANCE /tmp/$1 | awk '{print $15}'
}

function external_ip() {
  ensure_cached $1
  grep INSTANCE /tmp/$1 | awk '{print $14}'
}

function external_name() {
  ensure_cached $1
  grep INSTANCE /tmp/$1 | awk '{print $4}'
}

function internal_name() {
  ensure_cached $1
  grep INSTANCE /tmp/$1 | awk '{print $5}'
}

VARNISH=i-d0d38399
NODE1=i-cc1f81ba
NODE2=i-9c93c7ea
DB=i-7ae4b933

now=`date +%Y%m%d_%H%M%S`


if [ $# -gt 0 ]; then
  if [ $1 == "--clean" ]; then
    rm -f /tmp/$VARNISH /tmp/$NODE1 /tmp/$NODE2 /tmp/$DB
    exit
  fi
fi


varnishInt=`internal_ip $VARNISH`
varnishExt=`external_name $VARNISH`
varnishExtIp=`external_ip $VARNISH`

node1Int=`internal_ip $NODE1`
node1IntName=`internal_name $NODE1`
node1Ext=`external_name $NODE1`

node2Int=`internal_ip $NODE2`
node2IntName=`internal_name $NODE2`
node2Ext=`external_name $NODE2`


dbInt=`internal_ip $DB`
dbExt=`external_name $DB`

echo "Using node1[$node1Int] and node2[$node2Int] in varnish/default.vcl ..."
# TODO: It doesn't work yet
ssh morph@$varnishExt "sudo cp /etc/varnish/default.vcl /etc/varnish/default.vcl.$now && sudo sed 's/\<10\.10\.10\.4\>/'$node1Int'/g' /etc/varnish/default.vcl.$now | sudo sed 's/\<10\.10\.10\.5\>/'$node2Int'/g' > /tmp/default.vcl && sudo mv /tmp/default.vcl /etc/varnish && sudo chown root:root /etc/varnish/default.vcl && sudo chmod g-r /etc/varnish/default.vcl"

echo "Using external[0.0.0.0] and internal[$varnishInt] in pound.cfg ..."
ssh morph@$varnishExt "sudo cp /etc/pound.cfg /etc/pound.cfg.$now && sudo sed 's/^     Address .*/     Address 0.0.0.0/g' /etc/pound.cfg.$now | sudo sed 's/^               Address .*/               Address '$varnishInt'/g' > /tmp/pound.cfg && sudo cp /tmp/pound.cfg /etc/pound.cfg && sudo chown root:root /etc/pound.cfg && sudo chmod g-r /etc/pound.cfg"

echo "Using db[$dbInt] in node1 /etc/hosts ..."
ssh morph@$node1Ext "sudo cp /etc/hosts /etc/hosts.$now && sudo sed 's/.*# DB-InternalIP/'$dbInt' db # DB-InternalIP/g' /etc/hosts.$now > /tmp/hosts && sudo mv /tmp/hosts /etc/hosts"

echo "Using db[$dbInt] in node2 /etc/hosts ..."
ssh morph@$node2Ext "sudo cp /etc/hosts /etc/hosts.$now && sudo sed 's/.*# DB-InternalIP/'$dbInt' db # DB-InternalIP/g' /etc/hosts.$now > /tmp/hosts && sudo mv /tmp/hosts /etc/hosts"

echo "Use [$node1IntName] and [$node2IntName] for the mysql user"
ssh morph@$dbExt "echo \"DELETE FROM mysql.user WHERE User='morphsuits' and Host like 'ip%compute%'; FLUSH PRIVILEGES; \" | mysql -u root -pcTRr3amEUy6N"
ssh morph@$dbExt "echo \"CREATE USER 'morphsuits'@'$node1IntName' IDENTIFIED BY 'morphsuits'; CREATE USER 'morphsuits'@'$node2IntName' IDENTIFIED BY 'morphsuits'; GRANT ALL ON morphsuits_live.* TO 'morphsuits'@'%'; FLUSH PRIVILEGES; \" | mysql -u root -pcTRr3amEUy6N"
