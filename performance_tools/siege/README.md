## Siege performance benchmarking

* Install Siege
  brew install siege

* Launch Siege performance/streess test

  cd $MOPH_TOOLS/performance_tools/siege/checkout
  siege -Rsiegerc

* Log will be created in $MOPH_TOOLS/performance_tools/siege/checkout/log/checkout.log

# Create new load tests

  Copy and rename entire folder
  modify url.txt file
  modify custom Siege parameters in siegerc file