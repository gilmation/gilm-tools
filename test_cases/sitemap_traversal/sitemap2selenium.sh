#!/bin/bash

if [ $# -ne 3 ]; then
  echo "Usage: `basename $0` sitemap_file home_page destination_folder"
  echo
  echo "       The selenium script is created as destination_folder.html"
  echo "       and the screenshots are stored in destination_folder"
  echo "       destination_folder must be an absolute path."
  exit -1
fi

function pageTest() {
  echo ' <tr>'
  echo '      <td>open</td>'
  echo '      <td>'$1'</td>'
  echo '      <td></td>'
  echo ' </tr>'
  echo ' <tr>'
  echo '      <td>captureEntirePageScreenshot</td>'
  echo '      <td>'$2'.jpg</td>'
  echo '      <td></td>'
  echo ' </tr>'
}

{
echo '<?xml version="1.0" encoding="UTF-8"?>'
echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">'
echo '<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">'
echo '<head profile="http://selenium-ide.openqa.org/profiles/test-case">'
echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />'
echo '<link rel="selenium.base" href="'$2'" />'
echo '<title>royal</title>'
echo '</head>'
echo '<body>'
echo '<table cellpadding="1" cellspacing="1" border="1">'
echo '<thead>'
echo '<tr><td rowspan="1" colspan="3">'$3'</td></tr>'
echo '</thead><tbody>'

mkdir -p $3
i=1

pageTest $2 $3'/0'

for page in `grep "<loc>" $1 | sed 's/<loc>//g' | sed 's/<\/loc>//g'`; do
  pageTest $page $3'/'$i
  i=$[i+1]
done

echo '</tbody></table>'
echo '</body>'
echo '</html>'
} > $3.html

