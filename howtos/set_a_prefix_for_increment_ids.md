See Morphsuits/Eav:

    class Morphsuits_Eav_Model_Entity_Store extends Mage_Eav_Model_Entity_Store {
      public function readInstallationPrefix() {
        return Mage::getConfig()->getNode('default/eav/morphsuits')->store_prefix;
      }

    public function setIncrementPrefix($prefix) {
      $result = $this->setData("increment_prefix", $this->readInstallationPrefix() . $prefix);
      return $result;
    }
  }


set prefix in etc/config.xml


Make sure that the table eav_entity_store contains no data or check (if it is a new installation based on an old one)

    +-----------------+----------------+----------+------------------+-------------------+
    | entity_store_id | entity_type_id | store_id | increment_prefix | increment_last_id |
    +-----------------+----------------+----------+------------------+-------------------+
    |               2 |              6 |        1 | 661              | 66100000002       |
    |               3 |              5 |        1 | 661              | 66100000001       |
    +-----------------+----------------+----------+------------------+-------------------+

Notice that:

    mysql> select entity_type_code from eav_entity_type;
    +------------------+
    | entity_type_code |
    +------------------+
    | catalog_category |
    | catalog_product  |
    | creditmemo       |
    | customer         |
    | customer_address |
    | invoice          |
    | order            |
    | shipment         |
    +------------------+

