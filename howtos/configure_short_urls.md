To let Magento use short URLs in an already published site, we need to perform two steps:

## Configure short URLs

    System -> Configuration -> CATALOGUE (Catalogue) -> Search Engine Optimizations

Disable _Use Categories Path for Product URLs_.

## Define 301-redirects from the old urls to the new ones:
 
### Morphsuits

    RewriteCond %{REQUEST_URI} ^/morphsuits/(.+-original-morphsuit) [OR]
    RewriteCond %{REQUEST_URI} ^/morphsuits/pattern/(.+) [OR]
    RewriteCond %{REQUEST_URI} ^/morphsuits/flag/(.+) [OR]
    RewriteCond %{REQUEST_URI} ^/morphsuits/halloween/(.+) [OR]
    RewriteCond %{REQUEST_URI} ^/morphsuits/christmas/(.+) [OR]
    RewriteCond %{REQUEST_URI} ^/morphsuits/morphkids/(.+) [OR]
    RewriteCond %{REQUEST_URI} ^/suits/(.+-coloured) [OR]
    RewriteCond %{REQUEST_URI} ^/suits/pattern/(.+) [OR]
    RewriteCond %{REQUEST_URI} ^/accessories/(.+)
    RewriteRule ^(.*)$ /%1 [L,R=301,QSA]

### Foulfashion

    RewriteCond %{REQUEST_URI} ^/foulfashion/(foul-fashion-.*) [OR]
    RewriteCond %{REQUEST_URI} ^/foulfashion/(girls-foul-fashion-.*) [OR]
    RewriteCond %{REQUEST_URI} ^/accessories/(foul-fashion-.*) [OR]
    RewriteCond %{REQUEST_URI} ^/boys/(foul-fashion-.*) [OR]
    RewriteCond %{REQUEST_URI} ^/girls/(girls-foul-fashion-.*)
    RewriteRule ^(.*)$ /%1 [L,R=301,QSA]
