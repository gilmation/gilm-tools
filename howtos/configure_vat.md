## VAT configuration

Tax classes configuration can be found at Google Docs

Currently most of our stores (except Canada) use a price that includes taxes and we apply taxes based on the delivery address.

Using this setting, what Magento does is:

 * Product prices in the catalog are shown with the included tax added (i.e. the actual price found at the products configuration is used)
 * Once the quote is created (product added to the basket) it stores:
  * The price without VAT (the VAT used is the one corresponding to the Store Tax Zone & Rate country)
  * The VAT to apply
 * During checkout, when the delivery information step is completed, the store VAT class is discarded and the one corresponding to the final address is used.

This behaviour introduced several problems for our configuration:

 * If UK delivers to a distinct country without VAT class, the final price does not include VAT.
 * If UK delivers to a distinct country whose VAT class is uses a lower percentage, the final price is less than the one we expected.

In order to overcome these problems, we use Morphsuits_Tax to override the standard behaviour so that, if no VAT class is found, the one associated with the store country is used.

## Open issues

Manage taxes (VAT) for shipping prices
Refine information shown on emails for payment methods
VAT should be calculated on the price of an order AFTER any discounts (Report: VAT -> Reports -> Sales -> Orders)
USA VAT for the Google Feed
