## royal and awesome issue [#78]

Due to the fact that RoyalAndAwesome products are organised by type of garment (shorts, trousers, flat-caps, etc) and by pattern style (pink ticket, Hawaii 5-0, Well Plaid tartan, etc), urls with full category information are more suitable for the global configuration as they avoid name collision.

    System -> Configuration -> CATALOGUE (Catalogue) -> Search Engine Optimizations

_Use Categories Path for Product URLs_ Enabled

i.e. for each model such as bright blocks, we have trousers, shorts and flat-caps:

    RPBB    RSBB    RHBB1111
    RPBD    RSBD    RHBD1111
    RPFW    RSFW    RHFW1111
    RPHB    RSHB    RHHB1111
    RPHR    RSHR    RHHR1111
    RPMD    RSMD    RHMD1111
    RPPP    RSPP    RHPP1111
    RPWT    RSWT    RHWT1111
    RPYT    RSYT    RHYT1111
    RPZZ    RSZZ    RHZZ1111

Morphsuits/Catalog/Model/Product/Url.php overrides the Url schema selection model.

This extension is needed because the same product can be accessed via different urls. For trousers we have: 

    bright-blocks
    mens/bright-blocks
    mens/golf-trousers/bright-blocks

With the extension, Magento always uses the deepest url:

    mens/golf-trousers/bright-blocks
    mens/golf-shorts/bright-blocks
    accessories/flat-caps/bright-blocks

As the old urls showed increment numbers and were not uniform, we decided to use Magento''s rewriting model instead of using htaccess.
The following redirects were applied:

    update core_url_rewrite set target_path='mens/golf-trousers/blue-diamond', options='RP' where product_id=1 and id_path<>'product/1/9';                                 
    update core_url_rewrite set target_path='mens/golf-trousers/macyell-tartan', options='RP' where product_id=13 and id_path<>'product/13/9';                             
    update core_url_rewrite set target_path='mens/golf-trousers/pink-putter', options='RP' where product_id=16 and id_path<>'product/16/9';                                
    update core_url_rewrite set target_path='mens/golf-trousers/zebra', options='RP' where product_id=19 and id_path<>'product/19/9';                                      
    update core_url_rewrite set target_path='mens/golf-trousers/hawaii-red', options='RP' where product_id=49 and id_path<>'product/49/9';                                 
    update core_url_rewrite set target_path='mens/golf-trousers/hawaii-blue', options='RP' where product_id=61 and id_path<>'product/61/9';                                
    update core_url_rewrite set target_path='mens/golf-trousers/fireworks', options='RP' where product_id=73 and id_path<>'product/73/9';                                  
    update core_url_rewrite set target_path='mens/golf-trousers/bright-blocks', options='RP' where product_id=85 and id_path<>'product/85/9';                              
    update core_url_rewrite set target_path='mens/golf-shorts/zebra', options='RP' where product_id=97 and id_path<>'product/97/10';                                       
    update core_url_rewrite set target_path='mens/golf-trousers/macwhite-tartan', options='RP' where product_id=103 and id_path<>'product/103/9';                          
    update core_url_rewrite set target_path='mens/golf-trousers/multi-diamond', options='RP' where product_id=115 and id_path<>'product/115/9';                            
    update core_url_rewrite set target_path='mens/golf-shorts/pink-putter', options='RP' where product_id=127 and id_path<>'product/127/10';                               
    update core_url_rewrite set target_path='mens/golf-shorts/multi-diamond', options='RP' where product_id=133 and id_path<>'product/133/10';                             
    update core_url_rewrite set target_path='mens/golf-shorts/macyell-tartan', options='RP' where product_id=139 and id_path<>'product/139/10';                            
    update core_url_rewrite set target_path='mens/golf-shorts/macwhite-tartan', options='RP' where product_id=145 and id_path<>'product/145/10';                           
    update core_url_rewrite set target_path='mens/golf-shorts/hawaii-red', options='RP' where product_id=151 and id_path<>'product/151/10';                                
    update core_url_rewrite set target_path='mens/golf-shorts/hawaii-blue', options='RP' where product_id=157 and id_path<>'product/157/10';                               
    update core_url_rewrite set target_path='mens/golf-shorts/firework', options='RP' where product_id=163 and id_path<>'product/163/10';                                  
    update core_url_rewrite set target_path='mens/golf-shorts/bright-blocks', options='RP' where product_id=169 and id_path<>'product/169/10';                             
    update core_url_rewrite set target_path='mens/golf-shorts/blue-diamond', options='RP' where product_id=175 and id_path<>'product/175/10'; 

    update core_url_rewrite set target_path='accessories/flat-caps/bright-blocks', options='RP' where product_id=189 and id_path<>'product/189/7';                         
    update core_url_rewrite set target_path='accessories/flat-caps/blue-diamond', options='RP' where product_id=190 and id_path<>'product/190/7';                          
    update core_url_rewrite set target_path='accessories/flat-caps/firework', options='RP' where product_id=188 and id_path<>'product/188/7';                              
    update core_url_rewrite set target_path='accessories/flat-caps/hawaii-blue', options='RP' where product_id=187 and id_path<>'product/187/7';                           
    update core_url_rewrite set target_path='accessories/flat-caps/hawaii-red', options='RP' where product_id=186 and id_path<>'product/186/7';                            
    update core_url_rewrite set target_path='accessories/flat-caps/multi-diamond', options='RP' where product_id=183 and id_path<>'product/183/7';                         
    update core_url_rewrite set target_path='accessories/flat-caps/pink-putter', options='RP' where product_id=182 and id_path<>'product/182/7';                           
    update core_url_rewrite set target_path='accessories/flat-caps/macwhite-tartan', options='RP' where product_id=185 and id_path<>'product/185/7';                       
    update core_url_rewrite set target_path='accessories/flat-caps/macyell-tartan', options='RP' where product_id=184 and id_path<>'product/184/7';                        
    update core_url_rewrite set target_path='accessories/flat-caps/zebra', options='RP' where product_id=181 and id_path<>'product/181/7';


