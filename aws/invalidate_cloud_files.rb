require 'rubygems'
require 'hmac-sha1' # on OS X: sudo gem install ruby-hmac
require 'net/https'
require 'base64'

if ARGV[0] =~ /\-help/
  puts "usage: aws_cf_invalidate.rb [file1.html dir1/file2.jpg ...]"
  puts "File list is optional... not specifying a list will return current status of recent invalidation requests"
  exit
elsif ARGV[0] =~ /\-check/
  target_urls = nil
else 
# Target URLs
target_urls = [ '/skin/frontend/morphsuits/default/css/style.css',
                '/skin/frontend/morphsuits/default/css/translations.css',
                '/skin/frontend/base/default/css/widgets.css',
                '/skin/frontend/morphsuits/default/css/clipboard.css',
                '/skin/frontend/morphsuits/default/css/print.css',
                '/media/js/0b54ae2e2a3097743f1fecf29bdaec1e.js',
                '/media/js/f555bffc18e85c36161293bf1692849f.js',
                '/media/js/3c4be3a67ba25cb0dfe24455ada4b88b.js',
                '/media/js/a0f6905b1354a55d20939fc686f04d0a.js',
                '/media/js/3edf0c3f64d1c0465d5f1886ed9afa86.js',
                '/media/js/49030a80dacaec3dc6b3b72bc9f470dc.js',
                '/media/js/dec08031e8be676a1b49b7658720d2d6.js',
                '/media/js/9191d0f3026eeab110f9436ecb67e408.js',
                '/media/js/85f9ed905a3376ec91d29ba5f25c6f30.js',
                '/media/js/48b7f7fae79f89c53af406b8bf64e2d7.js',
                '/media/js/19ba5b12ac7f0f6628b6b5fca8981217.js',
                '/media/js/8a09000d030894ca696f1c4e7cdc5426.js' ]
end

# S3 Credentials
s3_access = 'AKIAJH4ZGAF66QGLTGOQ'
s3_secret = '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'

# Distributions
uk = 'EMA7SWJN66KTV'
fr = 'E124U2N3QNCR9K'
nl = 'E30JMG1C97TDVE'
nz = 'E147KISHTMSFNX'
ca = 'E3M9CPG9AH3F9K'
de = 'E1DYJFIUFI7OCD'
au = 'E1029CS9QMLMXB'
us = 'EHG3OEY34UUFF'
ar = 'E1M15QBFGZTY63'
se = 'E3BVLYZ1EJDIRO'
dk = 'EZTBSND9CCW7G'
es = 'EA81ZV5NH8CHY'
it = 'E37VVTNQTMWQJ8'
cf_distributions = [uk, fr, nl, nz, ca, de, au, us, ar]

if target_urls.nil? || target_urls.empty?
  paths = nil
else
  paths = '<Path>' + target_urls.join('</Path><Path>') + '</Path>'
end

cf_distributions.each do | cf_distribution |

  date = Time.now.utc
  date = date.strftime("%a, %d %b %Y %H:%M:%S %Z")
  digest = HMAC::SHA1.new(s3_secret)
  digest << date

  uri = URI.parse('https://cloudfront.amazonaws.com/2010-11-01/distribution/' + cf_distribution + '/invalidation')

  if paths != nil
    req = Net::HTTP::Post.new(uri.path)
  else
    req = Net::HTTP::Get.new(uri.path)
  end

  req.initialize_http_header({
    'x-amz-date' => date,
    'Content-Type' => 'text/xml',
    'Authorization' => "AWS %s:%s" % [s3_access, Base64.encode64(digest.digest)]
  })

  if paths != nil
    req.body = "<InvalidationBatch>" + paths + "<CallerReference>ref_#{Time.now.utc.to_i}</CallerReference></InvalidationBatch>"
  end

  http = Net::HTTP.new(uri.host, uri.port)
  http.use_ssl = true
  http.verify_mode = OpenSSL::SSL::VERIFY_NONE
  res = http.request(req)

  # TODO: Check status code and pretty print the output
  # Tip: pipe the output to | xmllint -format - |less for easier reading
  #puts $STDERR res.code
  puts res.body

end
