#!/bin/bash
#
# Check the robots.txt files and the sitemap.xml file that
# they should reference
#

sites='morphsuits.co.uk morphsuits.com morphsuits.com.au morphsuits-canada.com morphsuitsdeutschland.com morphsuits.nl morphsuits.fr morphsuits.it morphsuits.es royalandawesome.com.au royalandawesome.com royalandawesome.co.uk foulfashion.com.au foulfashion.com foulfashion.co.uk foulfashioncanada.com'

for s in $sites; do
  target="https://www.$s/contacts"
  #echo
  #echo "Checking [$target]"
  #echo
  status=$(curl -s -o /dev/null -w '%{http_code}' $target)
  echo "Status for [$target] was [$status]"
  #echo
  #echo "------------------"
done